﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using TuMvcApi.Data;
using AutoMapper;
using Microsoft.AspNetCore.Mvc.Cors.Internal;
using Pomelo.EntityFrameworkCore.MySql.Infrastructure;

namespace TuMvcApi
{
	public class Startup
	{
		private readonly IHostingEnvironment _env;
		public Startup(IConfiguration configuration, IHostingEnvironment env)
		{
			Configuration = configuration;
			_env = env;
		}

		readonly string MyAllowSpecificOrigins = "_myAllowSpecificOrigins";

		public IConfiguration Configuration { get; }

		// This method gets called by the runtime. Use this method to add services to the container.
		public void ConfigureServices(IServiceCollection services)
		{
			//services.AddDbContext<TuMvcDbContext>(options =>
			//		options.UseSqlServer(Configuration.GetConnectionString("HerokuMySql")));

			//_env.EnvironmentName = "Production";
			if (_env.IsDevelopment())
			{
				services.AddDbContext<TuMvcDbContext>(options =>
						options.UseSqlServer(Configuration.GetConnectionString("HomeDefault")));
			}
			else
			{
				services.AddDbContext<TuMvcDbContext>(options =>
				options.UseMySql(Configuration.GetConnectionString("HerokuMySql"),
				mysqlOptions =>
				{
					mysqlOptions.ServerVersion(new Version(5, 5, 56), ServerType.MySql);
				})
				);
			}
			

			services.AddScoped<ICourseRepository, CourseRepository>();
			services.AddScoped<ICategoryRepository, CategoryRepository>();

			services.AddScoped<IFileUploader, ImageUploader>();
			services.AddAutoMapper();
			services.AddCors(options =>
			{
				options.AddPolicy(MyAllowSpecificOrigins,
				builder =>
				{
					builder.WithOrigins(
						Configuration["ApiAllowedOrigin:DefaultLocalhost"].ToString(),
							Configuration["ApiAllowedOrigin:Heroku"].ToString()
						)
						.AllowAnyHeader()
						.AllowAnyMethod(); ;
				});
			});

			services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_1);
		}

		// This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
		public void Configure(IApplicationBuilder app, IHostingEnvironment env)
		{
			if (env.IsDevelopment())
			{
				app.UseDeveloperExceptionPage();
			}
			else
			{
				app.UseHsts();
			}

			app.UseStaticFiles();
			app.UseCors(MyAllowSpecificOrigins);
			app.UseHttpsRedirection();
			app.UseMvc();
		}
	}
}
