﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TuMvcApi.Models
{
	public class UploadedCourseImageModel
	{
		public string FileName { get; set; }
		public string FileLocation { get; set; }
	}
}
