﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;
using TuMvcApi.Data;

namespace TuMvcApi.Models
{
	public class CourseModel
	{
		public int? Id { get; set; }

		[Required]
		public string Title { get; set; }

		[Required]
		public int? CategoryId { get; set; }
		
		public CategoryModel Category { get; set; }

		[Required]
		[StringLength(300)]
		public string Description { get; set; }

		public int AccessMode { get; set; }

		[Required]
		[Range(typeof(DateTime), "01/01/1900", "01/01/2100", ErrorMessage = "Start Date is not valid")]
		public DateTime? StartDate { get; set; }

		[Required]
		[Range(typeof(DateTime), "01/01/1900", "01/01/2100", ErrorMessage = "End Date is not valid")]
		public DateTime? EndDate { get; set; }

		public int? PublishedBy { get; set; }

		public DateTime DatePublished { get; set; }
		
		public DateTime DateModified { get; set; }

		public int UpdatedBy { get; set; }

		[Required]
		public int? Duration { get; set; }
		
		public string CourseImage { get; set; }

		[Required(ErrorMessage = "Please select course type.")]
		public int? CourseTypeId { get; set; }

		public bool HideFromList { get; set; }

		[NotMapped]
		public string CourseType {
			get { return Enum.GetName(typeof(CourseTypes), CourseTypeId); }
			set { }
		}

		public enum CourseTypes
		{
			Scorm = 1,
			NonScorm = 2
		}
	}
}
