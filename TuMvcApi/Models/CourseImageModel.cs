﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TuMvcApi.Models
{
	public class CourseImageModel
	{
		public string FileName { get; set; }
		public string VirtualFilePath { get; set; }
		public string FileLocation { get; set; }
		public IFormFile File { get; set; }
	}
}
