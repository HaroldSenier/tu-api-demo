﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace TuMvcApi.Models
{
	public class CategoryModel
	{
		public int Id { get; set; }

		public int? ParentId { get; set; }

		[Required(ErrorMessage = "Name is required")]
		[StringLength(50)]
		public string Name { get; set; }

		public CategoryModel Parent { get; set; }
	}
}
