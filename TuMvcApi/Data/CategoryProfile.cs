﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TuMvcApi.Models;

namespace TuMvcApi.Data
{
	public class CategoryProfile : Profile
	{
		public CategoryProfile()
		{
			this.CreateMap<Category, CategoryModel>()
				.ReverseMap();
			
		}

	}
}
