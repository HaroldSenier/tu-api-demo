﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TuMvcApi.Models;

namespace TuMvcApi.Data
{
	public interface IFileUploader 
	{
		Task<UploadedCourseImageModel> UploadFile(CourseImageModel file);
	}
}
