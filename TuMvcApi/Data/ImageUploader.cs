﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using TuMvcApi.Models;

namespace TuMvcApi.Data
{
	public class ImageUploader : IFileUploader
	{
		public async Task<UploadedCourseImageModel> UploadFile(CourseImageModel file) {
			string locationToSave = file.FileLocation;
			
			using (var fs = new FileStream(locationToSave, FileMode.Create)) {

				await file.File.CopyToAsync(fs);
				UploadedCourseImageModel uploadedFile = new UploadedCourseImageModel
				{
					FileName = file.FileName,
					FileLocation = file.VirtualFilePath
					//FileLocation = String.Format("/Images/Course/{0}/{1}", file.CourseId, file.FileName)
				};
				return uploadedFile;
			}
		}			
	}
}
