﻿using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.ChangeTracking;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TuMvcApi.Data
{
	public class CourseRepository : ICourseRepository
	{
		private readonly TuMvcDbContext _db;
		private readonly ILogger<CourseRepository> _logger;

		public CourseRepository(TuMvcDbContext db, ILogger<CourseRepository> logger)
		{
			_db = db;
			_logger = logger;
		}

		public void Add<T>(T entity) where T : class
		{
			_logger.LogInformation($"Adding an object of type {entity.GetType()} to the context.");

			_db.Add(entity);
		}

		public void Delete<T>(T entity) where T : class
		{
			_logger.LogInformation($"Removing an object of type {entity.GetType()} to the context.");
			_db.Remove(entity);
		}

		public void Update<T>(T entity) where T : class
		{
			_logger.LogInformation($"Removing an object of type {entity.GetType()} to the context.");
			_db.Update(entity);
		}

		public async Task<bool> SaveChangesAsync()
		{
			_logger.LogInformation($"Attempitng to save the changes in the context");

			// Only return success if at least one row was changed
			return (await _db.SaveChangesAsync()) > 0;
		}

		//Course
		public void AddCourseAsync(Course course)
		{
			_db.AddAsync(course);
			//_db.SaveChanges();

		}

		public async Task<Course[]> GetAllCourseAsync(int? categoryId, int? subcategoryId)
		{
			_logger.LogInformation($"Getting all Course");
			Course[] courses;

			if (subcategoryId != null && categoryId != null)
			{
				courses = await _db.Course
						.Include(c => c.Category)
						.ThenInclude(c => c.Parent)
						.Where(c => c.HideFromList == false && c.Category.Id == subcategoryId)
						.ToArrayAsync();
			}
			else if (subcategoryId == null && categoryId != null)
			{
				courses = await _db.Course
						.Include(c => c.Category)
						.ThenInclude(c => c.Parent)
						.Where(c => c.HideFromList == false && c.Category.Id == categoryId)
						.ToArrayAsync();
			}
			else
			{
				courses = await _db.Course
						.Include(c => c.Category)
						.ThenInclude(c => c.Parent)
						.Where(c => c.HideFromList == false)
						.ToArrayAsync();
			}


			return courses;
		}

		public async Task<Course> GetCourseAsync(int courseId)
		{
			_logger.LogInformation($"Getting Course " + courseId.ToString());
			Course course;
			course = await _db.Course
						.Include(c => c.Category)
						.ThenInclude(c => c.Parent)
						.Where(c => c.HideFromList == false && c.Id == courseId)
						.SingleOrDefaultAsync();

			return course;
		}

		public async Task<Course[]> SearchCourse(string searchString)
		{
			_logger.LogInformation($"Search Course" + searchString);
			Course[] courses;

			courses = await _db.Course
					.Include(c => c.Category)
					.Where(c => c.HideFromList == false && (c.Title.Contains(searchString) || c.Description.Contains(searchString)))
					.ToArrayAsync();

			return courses;
		}

		public async Task<bool> CourseIdExists(int id)
		{
			_logger.LogInformation($"Get CourseIdExists " + id);
			bool exists = false;
			exists = await _db.Course.AnyAsync(c => c.Id == id);

			return exists;
		}

		public async Task<bool> HideCourseFromList(int id)
		{
			_logger.LogInformation($"Get HideCourseFromList " + id);
			Course course = new Course()
			{
				Id = id,
				HideFromList = true
			};

			_db.Course.Attach(course);
			_db.Entry(course).Property(x => x.HideFromList).IsModified = true;

			return (await _db.SaveChangesAsync()) > 0;
		}

	
	}
}
