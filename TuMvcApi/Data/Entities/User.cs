﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace TuMvcApi.Data
{
	public class User
	{
		[Key]
		public int UserId { get; set; }
		public string Email { get; set; }
		public int CIM { get; set; }
		public int UserRoleId { get; set; }

	}
}
