﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace TuMvcApi.Data
{
	public class Course
	{
		public Course()
		{
			HideFromList = false;
			DatePublished = DateTime.UtcNow;
			DateModified = DateTime.UtcNow;
		}

		[Key]
		public int Id { get; set; }

		[StringLength(100)]
		public string Title { get; set; }

		public int CategoryId { get; set; }

		[ForeignKey("CategoryId")]
		public Category Category { get; set; }
		
		[StringLength(300)]
		public string Description { get; set; }

		public int AccessMode { get; set; }

		public DateTime StartDate { get; set; }

		public DateTime EndDate { get; set; }

		public int PublishedBy { get; set; }

		public DateTime DatePublished { get; set; }
		
		public DateTime DateModified { get; set; }

		public int UpdatedBy { get; set; }

		public int Duration { get; set; }

		public string CourseImage { get; set; }

		public int CourseTypeId { get; set; }

		public bool HideFromList { get; set; }
		
	}
}
