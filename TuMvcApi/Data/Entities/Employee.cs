﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace TuMvcApi.Data
{
	public class Employee
	{
		[Key]
		public int CIM { get; set; }
		public string FirstName { get; set; }
		public string LastName { get; set; }
		public string TranscomEmail { get; set; }
		public string PersonalEmail { get; set; }
		public int Gender { get; set; }
		public string Role { get; set; }
		public string Department { get; set; }
		public int ReportsTo { get; set; }
	}
}
