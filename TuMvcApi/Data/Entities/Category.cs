﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace TuMvcApi.Data
{
	public class Category
	{
		public Category()
		{
			HideFromList = false;
		}

		[Key]
		public int Id { get; set; }

		[StringLength(50)]
		public string Name { get; set; }

		public int? ParentId { get; set; }

		[ForeignKey("ParentId")]
		public virtual Category Parent { get; set; }

		public bool HideFromList { get; set; }

	}
}
