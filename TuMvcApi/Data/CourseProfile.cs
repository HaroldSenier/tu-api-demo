﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TuMvcApi.Models;

namespace TuMvcApi.Data
{
	public class CourseProfile : Profile
	{
		public CourseProfile()
		{
			this.CreateMap<Course, CourseModel>()
				.ReverseMap();

			this.CreateMap<CourseModel, Course>()
				.ReverseMap()
				.ForMember(t => t.CourseType, opt => opt.MapFrom(c => c.CourseTypeId == 1 ? "Scorm" : "NonScorm"));
		}
	}
}
