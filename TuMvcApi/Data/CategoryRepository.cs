﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TuMvcApi.Data
{
	public class CategoryRepository : ICategoryRepository
	{
		private readonly TuMvcDbContext _db;
		private readonly ILogger<CategoryRepository> _logger;

		public CategoryRepository(TuMvcDbContext db, ILogger<CategoryRepository> logger)
		{
			_db = db;
			_logger = logger;
		}

		public void Add<T>(T entity) where T : class
		{
			_logger.LogInformation($"Adding an object of type {entity.GetType()} to the context.");

			_db.Add(entity);
		}

		public void Delete<T>(T entity) where T : class
		{
			_logger.LogInformation($"Removing an object of type {entity.GetType()} to the context.");
			_db.Remove(entity);
		}

		public void Update<T>(T entity) where T : class
		{
			_logger.LogInformation($"Removing an object of type {entity.GetType()} to the context.");
			_db.Update(entity);
		}

		public async Task<bool> SaveChangesAsync()
		{
			_logger.LogInformation($"Attempitng to save the changes in the context");

			// Only return success if at least one row was changed
			return (await _db.SaveChangesAsync()) > 0;
		}

		//Category
		public async Task<Category[]> GetAllCategoryAsync(string filter, int? parentCategoryId)
		{
			_logger.LogInformation($"Getting all Category");
			Category[] categories;

			//filter=all
			//filter=parentOnly
			//filter=byCategory
			//filter=all&parentId=1 
			//filter=parentOnly&parentId=1
			//filter=byCategory&parentId=1 or parentId=1
			//
			//index

			//filter = filter != "" ? filter = filter.ToLower() : filter;

			if ((parentCategoryId != null) || (filter == "bycategory" && parentCategoryId != null))
			{
				categories = await _db.Category
					.Include(c => c.Parent)
					.Where(c => c.ParentId == parentCategoryId && c.HideFromList == false)
					.ToArrayAsync();
			}
			else if (filter == "parentonly")
			{
				categories = await _db.Category
					.Include(c => c.Parent)
					.Where(c => c.ParentId == null && c.HideFromList == false)
					.ToArrayAsync();
			}
			else if (filter == "all" || (filter == null && parentCategoryId == null))
			{
				//all or empty
				//get all categories
				categories = await _db.Category
					.Include(c => c.Parent)
					.Where(c => c.HideFromList == false)
					.ToArrayAsync();
			}
			else
				categories = null;

			return categories;
		}

		public async Task<Category> GetCategoryAsync(int categoryId)
		{
			_logger.LogInformation($"Getting all Category");
			Category category;
			category = await _db.Category
						.Include(c => c.Parent)
						.Where(c => c.Id == categoryId)
						.SingleOrDefaultAsync();

			return category;
		}

		public async Task<bool> CategoryNameExists(string name, int? parentId)
		{
			_logger.LogInformation($"Get CategoryNameExists " + name);
			bool exists = false;
			exists = await _db.Category.AnyAsync(c => c.Name == name && c.ParentId == parentId && c.HideFromList == false);

			return exists;
		}

		public async Task<bool> HideCategoryFromList(int id)
		{
			_logger.LogInformation($"Get CategoryIdExists " + id);
			Category newCategory = new Category()
			{
				Id = id,
				HideFromList = true
			};

			_db.Category.Attach(newCategory);
			_db.Entry(newCategory).Property(x => x.HideFromList).IsModified = true;

			return (await _db.SaveChangesAsync()) > 0;
		}

		public async Task<bool> CategoryIdExists(int id)
		{
			_logger.LogInformation($"Get CategoryIdExists " + id);
			bool exists = false;
			exists = await _db.Category.AnyAsync(c => c.Id == id);

			return exists;
		}

		//Subcategory
		//public async Task<Subcategory[]> GetAllSubCategoryAsync()
		//{
		//	_logger.LogInformation($"Getting all Subcategory");
		//	Subcategory[] subcategories;
		//	subcategories = await _db.Subcategory
		//				.ToArrayAsync();

		//	return subcategories;
		//}

		//public async Task<Subcategory> GetSubCategoryAsync(int subcategoryId)
		//{
		//	_logger.LogInformation($"Getting Subcategory " + subcategoryId);
		//	Subcategory subcategory;
		//	subcategory = await _db.Subcategory
		//				.Where(c => c.Id == subcategoryId)
		//				.SingleOrDefaultAsync();

		//	return subcategory;
		//}

		//public async Task<Subcategory[]> GetSubcategoryByCategory(int categoryId)
		//{
		//	_logger.LogInformation($"Getting Subcategory categoryId " + categoryId);
		//	Subcategory[] subcategories;
		//	subcategories = await _db.Subcategory
		//				.Include(c => c.Category)
		//				.Where(c => c.Category.Id == categoryId)
		//				.ToArrayAsync();

		//	return subcategories;
		//}
	}
}
