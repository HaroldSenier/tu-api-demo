﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TuMvcApi.Data
{
	public class TuMvcDbContext : DbContext
	{

		public TuMvcDbContext(DbContextOptions options) : base(options)
		{
		}

		public DbSet<Course> Course { get; set; }
		public DbSet<Category> Category { get; set; }
		public DbSet<Employee> Employee { get; set; }
		public DbSet<User> User { get; set; }
	}
}
