﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TuMvcApi.Data
{
	public interface ICourseRepository
	{
		// General 
		void Add<T>(T entity) where T : class;
		void Delete<T>(T entity) where T : class;
		void Update<T>(T entity) where T : class;
		Task<bool> SaveChangesAsync();

		//Course
		void AddCourseAsync(Course course);
		Task<Course[]> GetAllCourseAsync(int? categoryId, int? subcategoryId);
		Task<Course> GetCourseAsync(int courseId);
		Task<Course[]> SearchCourse(string searchString);
		Task<bool> CourseIdExists(int id);
		Task<bool> HideCourseFromList(int id);
		//Task<Course[]> GetAllCourseByCategoryAsync(int categoryId);


		

	}
}
