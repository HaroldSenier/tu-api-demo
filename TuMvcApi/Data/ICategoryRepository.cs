﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TuMvcApi.Data
{
	public interface ICategoryRepository
	{
		// General 
		void Add<T>(T entity) where T : class;
		void Delete<T>(T entity) where T : class;
		void Update<T>(T entity) where T : class;
		Task<bool> SaveChangesAsync();

		//Category
		Task<Category[]> GetAllCategoryAsync(string filter, int? filterByParentCategoryId);
		Task<Category> GetCategoryAsync(int categoryId);
		Task<bool> CategoryNameExists(string name, int? parentId);
		Task<bool> CategoryIdExists(int id);
		Task<bool> HideCategoryFromList(int id);
		////Subcategory
		//Task<Subcategory[]> GetAllSubCategoryAsync();
		//Task<Subcategory> GetSubCategoryAsync(int subcategoryId);
		//Task<Subcategory[]> GetSubcategoryByCategory(int categoryId);
	}
}
