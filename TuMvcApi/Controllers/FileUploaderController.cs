﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using TuMvcApi.Data;
using TuMvcApi.Models;

namespace TuMvcApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class FileUploaderController : ControllerBase
    {

		private readonly TuMvcDbContext _db;
		private readonly ILogger<FileUploaderController> _logger;
		private readonly IHostingEnvironment _he;
		private readonly IFileUploader _uploader;

		public FileUploaderController(TuMvcDbContext db, ILogger<FileUploaderController> logger, IHostingEnvironment he, IFileUploader uploader)
		{
			_db = db;
			_logger = logger;
			_he = he;
			_uploader = uploader;
		}


		//api/FileUploader/CourseImage
		[HttpPost]
		[Route("CourseImage")]
		public async Task<ActionResult<UploadedCourseImageModel>> UploadCourseImage(IFormFile file)
		{
			try
			{
				if (file != null)
				{
					string host = this.Request.Host.ToString();
					string timeStamp = DateTimeOffset.UtcNow.ToUnixTimeSeconds().ToString();
					//int courseId = Convert.ToInt32(Request.Form["courseId"]);
					string filename = timeStamp + Path.GetExtension(file.FileName);
					string saveToPath = Path.Combine(String.Format("{0}\\Images\\Course\\", _he.WebRootPath));
					string filePath = Path.Combine(saveToPath, filename);

					if (!Directory.Exists(saveToPath))
					{
						_logger.LogInformation($"Created Directory + " + saveToPath);
						// Try to create the directory.
						DirectoryInfo di = Directory.CreateDirectory(saveToPath);
					}

					CourseImageModel model = new CourseImageModel
					{
						FileName = filename,
						VirtualFilePath = String.Format("http://{0}/Images/Course/{1}", host, filename),
						FileLocation = filePath,
						File = file
					};


					return await _uploader.UploadFile(model);
				}
				else
				{
					return BadRequest("Please select an image file.");
				}
				
			}
			catch (Exception ex)
			{
				return this.StatusCode(StatusCodes.Status500InternalServerError, "Upload failure" + ex.ToString());
			}
		}

		[HttpGet]
		public IActionResult GetCourseImage(int courseId, string filename)
		{
			var image = System.IO.File.OpenRead(Path.Combine(new string[] { _he.WebRootPath, "Images" , "Course", courseId.ToString(), filename}));
			return File(image, "image/jpeg");
		}
	}
}