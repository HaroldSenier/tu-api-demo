﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using TuMvcApi.Data;
using TuMvcApi.Models;

namespace TuMvcApi.Controllers
{
	[Route("api/[controller]")]
	[ApiController]
	public class CategoriesController : ControllerBase
	{
		private readonly ICategoryRepository _repository;
		private readonly IMapper _mapper;

		public CategoriesController(ICategoryRepository repository, IMapper mapper)
		{
			_repository = repository;
			_mapper = mapper;
		}


		// GET: api/Categories?filter={filter}&parentId={categoryId}
		//filter :(all,byParent,parentOnly)
		[HttpGet]
		public async Task<ActionResult<Category[]>> GetAllCategory(string filter, int? parentId)
		{
			return await _repository.GetAllCategoryAsync(filter, parentId);
		}

		// POST: api/Categories/Create
		[HttpPost]
		[Route("Create")]
		public async Task<ActionResult<CategoryModel>> Create([FromBody] CategoryModel model)
		{
			try
			{
				if (await _repository.CategoryNameExists(model.Name, model.ParentId))
				{
					return BadRequest(String.Format("Category {0} already exists", model.Name));
				}
				else
				{
					// Create a new camp
					var category = _mapper.Map<Category>(model);
					_repository.Add(category);

					if (await _repository.SaveChangesAsync())
					{
						return Created($"/api/categories/{category.Name}", _mapper.Map<CategoryModel>(category));
					}
				}
			}
			catch (Exception ex)
			{
				return this.StatusCode(StatusCodes.Status500InternalServerError, "Database failure" + ex.ToString());
			}
			return BadRequest();

		}

		
		[HttpGet]
		[Route("Get")]
		public async Task<ActionResult<Category>> GetCategoryAsync(int id)
		{

			return await _repository.GetCategoryAsync(id);
		}

		[HttpPost]
		[Route("Update")]
		public async Task<ActionResult<CategoryModel>> Update([FromBody] CategoryModel model)
		{
			try
			{
				// Create a new category
				var category = _mapper.Map<Category>(model);
				_repository.Update(category);

				if (await _repository.SaveChangesAsync())
				{
					return Created($"/api/categories/{category.Name}", _mapper.Map<CategoryModel>(category));
				}
			}
			catch (Exception ex)
			{
				return this.StatusCode(StatusCodes.Status500InternalServerError, "Database failure" + ex.ToString());
			}
			return BadRequest();

		}

		[HttpDelete]
		[Route("Delete/{id}")]
		public async Task<ActionResult> Delete(int id)
		{
			try
			{
				// Create a new category
				bool exists = await _repository.CategoryIdExists(id);

				if (exists)
				{

					if (await _repository.HideCategoryFromList(id))
					{
						return Ok();
					}
				}
				else
				{
					return BadRequest(String.Format("The Category Id = {0}  Dont Exists ", id));
				}

			}
			catch (Exception ex)
			{
				return this.StatusCode(StatusCodes.Status500InternalServerError, "Database failure" + ex.ToString());
			}
			return BadRequest();

		}

		//[HttpPost]
		//[Route("Delete")]
		//public async Task<ActionResult> Update(int id)
		//{
		//	try
		//	{
		//		// Create a new category
		//		var category = _mapper.Map<Category>(model);
		//		_repository.Update(category);

		//		if (await _repository.SaveChangesAsync())
		//		{
		//			return Created($"/api/categories/{category.Name}", _mapper.Map<CategoryModel>(category));
		//		}
		//	}
		//	catch (Exception ex)
		//	{
		//		return this.StatusCode(StatusCodes.Status500InternalServerError, "Database failure" + ex.ToString());
		//	}
		//	return BadRequest();

		//}


		//// GET: api/Categories
		//[HttpGet]
		//public IEnumerable<Category> GetCategory()
		//{
		//    return _context.Category;
		//}

		//// GET: api/Categories/5
		//[HttpGet("{id}")]
		//public async Task<IActionResult> GetCategory([FromRoute] int id)
		//{
		//    if (!ModelState.IsValid)
		//    {
		//        return BadRequest(ModelState);
		//    }

		//    var category = await _context.Category.FindAsync(id);

		//    if (category == null)
		//    {
		//        return NotFound();
		//    }

		//    return Ok(category);
		//}

		//// PUT: api/Categories/5
		//[HttpPut("{id}")]
		//public async Task<IActionResult> PutCategory([FromRoute] int id, [FromBody] Category category)
		//{
		//    if (!ModelState.IsValid)
		//    {
		//        return BadRequest(ModelState);
		//    }

		//    if (id != category.Id)
		//    {
		//        return BadRequest();
		//    }

		//    _context.Entry(category).State = EntityState.Modified;

		//    try
		//    {
		//        await _context.SaveChangesAsync();
		//    }
		//    catch (DbUpdateConcurrencyException)
		//    {
		//        if (!CategoryExists(id))
		//        {
		//            return NotFound();
		//        }
		//        else
		//        {
		//            throw;
		//        }
		//    }

		//    return NoContent();
		//}

		//// POST: api/Categories
		//[HttpPost]
		//public async Task<IActionResult> PostCategory([FromBody] Category category)
		//{
		//    if (!ModelState.IsValid)
		//    {
		//        return BadRequest(ModelState);
		//    }

		//    _context.Category.Add(category);
		//    await _context.SaveChangesAsync();

		//    return CreatedAtAction("GetCategory", new { id = category.Id }, category);
		//}

		//// DELETE: api/Categories/5
		//[HttpDelete("{id}")]
		//public async Task<IActionResult> DeleteCategory([FromRoute] int id)
		//{
		//    if (!ModelState.IsValid)
		//    {
		//        return BadRequest(ModelState);
		//    }

		//    var category = await _context.Category.FindAsync(id);
		//    if (category == null)
		//    {
		//        return NotFound();
		//    }

		//    _context.Category.Remove(category);
		//    await _context.SaveChangesAsync();

		//    return Ok(category);
		//}

		//private bool CategoryExists(int id)
		//{
		//    return _context.Category.Any(e => e.Id == id);
		//}
	}
}