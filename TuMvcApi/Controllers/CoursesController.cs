﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using TuMvcApi.Data;
using TuMvcApi.Models;

namespace TuMvcApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CoursesController : ControllerBase
    {
		private readonly ICourseRepository _repository;
		private readonly IMapper _mapper;

		public CoursesController(ICourseRepository repository, IMapper mapper)
		{
			_repository = repository;
			_mapper = mapper;
		}

		// GET: api/Courses
		[HttpGet]
		public async Task<ActionResult<CourseModel[]>> GetAllCourse(int? categoryId, int? subCategoryId)
		{
			return  _mapper.Map<CourseModel[]>(await _repository.GetAllCourseAsync(categoryId, subCategoryId)) ;
		}

		[HttpGet("course")]
		[Route("get/{id}")]
		// GET: api/Courses/get/{id}
		public async Task<ActionResult<CourseModel>> GetCourseAsync(int id)
		{
			return  _mapper.Map<CourseModel>(await _repository.GetCourseAsync(id));
		}

		// POST: api/Courses/Create
		[HttpPost]
		[Route("Create")]
		public async Task<ActionResult<CourseModel>> Create([FromBody] CourseModel model)
		{
			try
			{
				if (!ModelState.IsValid)
				{
					return BadRequest("Model State is not valid");
				}
				else {
					// Create a new camp
					var course = _mapper.Map<Course>(model);
					//_repository.AddCourseAsync(course);
					_repository.Add(course);

					if (await _repository.SaveChangesAsync())
					{
						return Created($"/api/categories/{course.Title}", _mapper.Map<CourseModel>(course));
					}
				}

				return BadRequest();

			}
			catch (Exception ex)
			{
				return this.StatusCode(StatusCodes.Status500InternalServerError, "Database failure with Exception:" + ex);
			}
			

		}

		[HttpPost]
		[Route("Update")]
		public async Task<ActionResult<CourseModel>> Update([FromBody] CourseModel model)
		{
			try
			{
				if (!ModelState.IsValid)
				{
					return BadRequest("Model State is not valid");
				}
				else
				{

					if (await _repository.CourseIdExists((int)model.Id)) {
						// Create a new camp
						var course = _mapper.Map<Course>(model);
						//_repository.AddCourseAsync(course);
						_repository.Update(course);

						if (await _repository.SaveChangesAsync())
						{
							return Created($"/api/categories/{course.Title}", _mapper.Map<CourseModel>(course));
						}
					}
					
				}

				return BadRequest();

			}
			catch (Exception ex)
			{
				return this.StatusCode(StatusCodes.Status500InternalServerError, "Database failure with Exception:" + ex);
			}


		}

		[HttpDelete]
		[Route("Delete/{id}")]
		public async Task<ActionResult> Delete(int id)
		{
			try
			{
				// Create a new category
				bool exists = await _repository.CourseIdExists(id);

				if (exists)
				{

					if (await _repository.HideCourseFromList(id))
					{
						return Ok();
					}
				}
				else
				{
					return BadRequest(String.Format("The Course Id = {0}  Dont Exists ", id));
				}

			}
			catch (Exception ex)
			{
				return this.StatusCode(StatusCodes.Status500InternalServerError, "Database failure" + ex.ToString());
			}
			return BadRequest();

		}
		
		[HttpGet("search")]
		// GET: api/Courses/search?q=title
		public async Task<ActionResult<Course[]>> SearchCourse(string q)
		{
			return await _repository.SearchCourse(q);
		}
		


	}
}