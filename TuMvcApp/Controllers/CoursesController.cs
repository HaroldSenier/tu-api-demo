﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

namespace TuMvcApp.Controllers
{
    public class CoursesController : Controller
    {
        public IActionResult Index()
        {
            return View();
        }


		// GET: Courses/Create
		public IActionResult Create()
		{
			return View();
		}

		// GET: Courses/Catalog
		public IActionResult Catalog()
		{
			return View();
		}

		// GET: Courses/Search
		public IActionResult Search(string q)
		{
			return View();
		}
	}
}