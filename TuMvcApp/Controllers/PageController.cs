﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

namespace Tu_Mvc.Controllers
{
    public class PageController : Controller
    {
        public IActionResult Index()
        {
            return View("~/Views/Index.cshtml");
        }
	}
}