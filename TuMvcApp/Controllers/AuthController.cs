﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

namespace TuMvcApp.Controllers
{
	[Route("auth")]
	public class AuthController : Controller
	{
		[Route("signin")]
		public IActionResult signin(string returnUrl = null)
		{
			//return Challenge(new AuthenticationProperties { RedirectUri = returnUrl ?? "/home" });
			return Redirect("~/home");
		}
	}
}