﻿

function LoadCategoriesIntoTable(tableName) {
    apiGetCategories = apiEndpoint + "/Categories"
    $.ajax({
        contentType: 'application/json',
        type: "GET",
        url: apiGetCategories,
        crossDomain: true,
        success: function (data, textStatus, jqXHR) {
            console.log("Get Success.");
            console.log(data);
            $('#' + tableName).DataTable({
                "processing" : true,
                "data": data,
                "columns": [
                    { "data": "id" },
                    { "data": "name" },
                    { "data": "parentId" }
                ],
                "language": {
                    "sSearch": "Filter results:",
                    'processing': 'Loading...'

                }
            });
        },
        error: function (jqXHR, textStatus, errorThrown) {
            if (jqXHR.status == 0)
                return alert("Could not connect to server.");

            var responseMsg = jqXHR.responseJSON;
            console.log(responseMsg);
            console.log(errorThrown);
        }
    });
}

function CreateCategory() {
    let _name, _parentid;
    _name = $('#Name').val().trim();
    _parentid = parseInt($('#ddCategory').val());
    $.ajax({
        contentType: 'application/json',
        type: "POST",
        url: apiEndpoint + "/Categories/Create",
        crossDomain: true,
        data: JSON.stringify({
            Name: _name,
            ParentId: _parentid >= 0 ? _parentid : null
        }),
        success: function (data, textStatus, jqXHR) {
            var categoryName = data.name;
            console.log(data);
            console.log(jqXHR);
            swal({
                title: "Success",
                text: jqXHR.statusText,
                icon: "success",
                button: "Close",
            }).then((response) => {
                window.location = "/Categories";
            });
            //alert(jqXHR.statusText + " " + categoryName);

        },
        error: function (jqXHR, textStatus, errorThrown) {
            //if (jqXHR.status == "500")
            //    return alert(jqXHR.statusText);
            if (jqXHR.status == "400" && jqXHR.responseJSON != null) {
                var responseMsg = jqXHR.responseJSON;
                for (var error in responseMsg) {
                    var errorValue = responseMsg[error];
                    $("#validationSummary").html('');
                    $("#validationSummary").append("<li>" + errorValue + "</li>")
                }
            } else {
                swal({
                    title: "Error Encountered!",
                    text: jqXHR.responseText,
                    icon: "error",
                    button: "Ok",
                });

            }


        }
    });
}

function clearSubcategory() {
    $('#ddSubcategory').html('');
}

function clearCategory() {
    $('#ddCategory').html('');
}

function enableSelectSubcategory() {
    $("#ddSubcategory").disabled = false;
}

function disableSelectSubcategory() {
    $("#ddSubcategory").disabled = false;
}

function loadSubcategory(parentCategoryId) {
    apiGetCategories = apiEndpoint + "/Categories?parentId=";
    $.ajax({
        contentType: 'application/json',
        type: "GET",
        url: apiGetCategories + parentCategoryId,
        crossDomain: true,
        success: function (data, textStatus, jqXHR) {
            console.log(data);
            populateSelectSubcategory(data);
        },
        error: function (jqXHR, textStatus, errorThrown) {
            if (jqXHR.status == 0)
                return alert("Could not connect to server.");

            var responseMsg = jqXHR.responseJSON;
            console.log(responseMsg);
            console.log(errorThrown);
        }
    });
}

function loadCategory() {
    apiGetCategories = apiEndpoint + "/Categories";
    $.ajax({
        contentType: 'application/json',
        type: "GET",
        url: apiGetCategories,
        crossDomain: true,
        success: function (data, textStatus, jqXHR) {
            populateSelectCategory(data);
        },
        error: function (jqXHR, textStatus, errorThrown) {
            if (jqXHR.status == 0)
                return alert("Could not connect to server.");

            var responseMsg = jqXHR.responseJSON;
            console.log(responseMsg);
            console.log(errorThrown);
        }
    });
}

function populateSelectCategory(categories) {
    //add default select
    let select = $("#ddCategory");

    var option = document.createElement("option");
    option.text = "Select";
    option.value = -1;
    select.append(option);

    categories.map(function (c) {
        var option = document.createElement("option");
        option.text = c.name;
        option.value = c.id;
        select.append(option);
    });
}

function populateSelectSubcategory(subcategories) {
    let select = $("#ddSubcategory");
    let defaultSelectItem = "Select";
    if (subcategories.length == 0)
        defaultSelectItem = "No Subcategory Available";

    var option = document.createElement("option");
    option.text = defaultSelectItem;
    option.value = -1;
    select.append(option);

    subcategories.map(function (c) {
        var option = document.createElement("option");
        option.text = c.name;
        option.value = c.id;
        select.append(option);
    });
}

function addDefaultSelect(selectId, defaultItem) {
    let select = $("#" + selectId);
    var option = document.createElement("option");
    option.text = defaultItem;
    option.value = -1;
    select.append(option);
}