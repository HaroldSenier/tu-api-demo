﻿function loadSearchResult() {

    var urlParams = new URLSearchParams(window.location.search);
    var searchString = urlParams.get('q');
    apiSearchCourse = apiEndpoint + `/Courses/Search?q=${searchString}`;
    $("#txtSearch").val(searchString);
    $.get(apiSearchCourse)
        .done(function (courses) {
            $('#courseCatalogContainer').empty();

            if (courses.length > 0) {
                $.each(courses, function (i, course) {
                    var item = `<div class="col-md-4">
                                                <div class="card mb-4 addborder-on-hover">
                                                    <img class="card-img-top card-image" src="//placeimg.com/290/180/any" alt="Card image cap">
                                                    <div class="card-body">
                                                        <h5 class="card-title">${course.title}</h5>
                                                        <p class="card-text">${course.description}</p>
                                                        <a href="/" class="btn btn-dark btn-sm">Go somewhere</a>
                                                    </div>
                                                </div>
                                            </div>`;
                    $('#courseCatalogContainer').append(item);
                });
            } else {
                var item = `No result found.`;
                $('#courseCatalogContainer').append(item);
            }


        })
        .fail(function (jqXHR, textStatus, errorThrown) {
            swal({
                title: "Error",
                text: jqXHR.statusText,
                icon: "error",
                button: "Close",
            });
        })

}

function searchCourse(searchString) {
    window.location.href = "/Courses/Search?q=" + searchString;
}