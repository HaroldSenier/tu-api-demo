﻿
function initializeDatePickers() {
    var dps = ["dpEnddate", "dpStartdate"]
    dps.map(function (dp) {
        $("#" + dp).datetimepicker({
            format: 'L',
            useCurrent: false
        });
    });
}

function CreateCourse() {

    let Title = $("#txtTitle").val().trim();
    let CategoryId = $("#ddSubcategory").val();
    let Description = $("#txtDescription").val().trim();
    let AccessMode = $("#ddAccessMode").val().trim();
    let StartDate = $("#txtStartdate").val().trim();
    let EndDate = $("#txtEnddate").val().trim();
    let PublishedBy = $("#txtPublishedBy").val().trim();
    let UpdatedBy = "10146063";
    let Duration = $("#txtDuration").val().trim();
    let CourseImage = $("#txtCourseImage").val().trim();
    let CourseTypeId = $("#ddCourseType").val().trim();

    //TEsting
    //Title = "Course test 1";
    //CategoryId = 1;
    //Description = "lorem";
    //AccessMode = 1;
    //StartDate = "";
    //EndDate = "";
    //PublishedBy = "";
    //UpdatedBy = "";
    //Duration = "";
    //CourseImage = "";
    //CourseTypeId = "";
    $.ajax({
        contentType: 'application/json',
        type: "POST",
        url: apiEndpoint + "/Courses/Create",
        crossDomain: true,
        data: JSON.stringify({
            "Title": Title,
            "CategoryId": CategoryId,
            "Description": Description,
            "AccessMode": AccessMode,
            "StartDate": StartDate,
            "EndDate": EndDate,
            "PublishedBy": PublishedBy,
            "UpdatedBy": UpdatedBy,
            "Duration": Duration,
            "CourseImage": CourseImage,
            "CourseTypeId": CourseTypeId
        }),
        success: function (data, textStatus, jqXHR) {
            var categoryName = data.name;
            console.log(data);
            console.log(jqXHR);
            swal({
                title: "Success",
                text: jqXHR.statusText,
                icon: "success",
                button: "Close",
            }).then((response) => {
                window.location = "/Courses/Catalog";
            });
            //alert(jqXHR.statusText + " " + categoryName);

        },
        error: function (jqXHR, textStatus, errorThrown) {
            //if (jqXHR.status == "500")
            //    return alert(jqXHR.statusText);
            if (jqXHR.status == "400" && jqXHR.responseJSON != null) {
                var responseMsg = jqXHR.responseJSON;
                for (var error in responseMsg) {
                    var errorValue = responseMsg[error];
                    $("#validationSummary").html('');
                    $("#validationSummary").append("<li>" + errorValue + "</li>")
                }
            } else {
                swal({
                    title: "Error Encountered!",
                    text: jqXHR.statusText,
                    icon: "error",
                    button: "Ok",
                });

            }
        }
    });
}